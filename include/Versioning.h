#ifndef VERSIONING_H
#define VERSIONING_H
#include <stdint.h>
#include <cstring>

struct Version {
	uint16_t Major;
	uint16_t Minor;
	uint16_t Patch;

	Version() { memset(this, 0, sizeof(Version)); }
};

#endif // !VERSIONNING_H
